package com.example.wisyst.androidmvp.ui.feature.login;

/**
 * Created by wisyst on 6/11/2018.
 */

public interface LoginPresenter {

    void validateUser(String name, String pass);

    void onDestroy();
}
