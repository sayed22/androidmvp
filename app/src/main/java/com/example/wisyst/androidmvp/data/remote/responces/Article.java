
package com.example.wisyst.androidmvp.data.remote.responces;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Article implements Serializable {

    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("post_create_date")
    @Expose
    private String postCreateDate;
    @SerializedName("post_views_number")
    @Expose
    private String postViewsNumber;
    @SerializedName("post_image_url")
    @Expose
    private String postImageUrl;
    @SerializedName("content_brief")
    @Expose
    private String contentBrief;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostCreateDate() {
        return postCreateDate;
    }

    public void setPostCreateDate(String postCreateDate) {
        this.postCreateDate = postCreateDate;
    }

    public String getPostViewsNumber() {
        return postViewsNumber;
    }

    public void setPostViewsNumber(String postViewsNumber) {
        this.postViewsNumber = postViewsNumber;
    }

    public String getPostImageUrl() {
        return postImageUrl;
    }

    public void setPostImageUrl(String postImageUrl) {
        this.postImageUrl = postImageUrl;
    }

    public String getContentBrief() {
        return contentBrief;
    }

    public void setContentBrief(String contentBrief) {
        this.contentBrief = contentBrief;
    }

}
