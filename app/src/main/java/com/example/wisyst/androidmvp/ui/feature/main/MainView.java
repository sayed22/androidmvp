package com.example.wisyst.androidmvp.ui.feature.main;

import com.example.wisyst.androidmvp.data.remote.responces.ArticlesResponce;

/**
 * Created by wisyst on 6/11/2018.
 */

public interface MainView {

    void onResponseRecevied(ArticlesResponce articlesResponce);

    void onErrorReceived(String message);

    void showProgress();

    void hideProgress();
}
