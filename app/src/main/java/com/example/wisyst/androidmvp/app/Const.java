package com.example.wisyst.androidmvp.app;

/**
 * Created by wisyst on 3/5/2018.
 */

public class Const {
    public static final String SHARED_PREFERENCES_FILE_NAME = "Khaybar";
    public static final String BASE_URL = "http://khaybarnews.com/api/";
    public static final String QUERY_PARAM_EMAIL_NAME = "api_email";
    public static final String QUERY_PARAM_EMAIL_VALUE = "api_email@api_email.com";
    public static final String QUERY_PARAM_PASSWORD_NAME = "api_password";
    public static final String QUERY_PARAM_PASSWORD_VALUE = "P@ssww07#d";
    public static final String API_GET_SECTIONS = "GetCategoryPostsList";
}
