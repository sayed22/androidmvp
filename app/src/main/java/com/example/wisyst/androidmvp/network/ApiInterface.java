package com.example.wisyst.androidmvp.network;


import com.example.wisyst.androidmvp.app.Const;
import com.example.wisyst.androidmvp.data.remote.requests.SectionsRequest;
import com.example.wisyst.androidmvp.data.remote.responces.ArticlesResponce;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {


    @POST(Const.API_GET_SECTIONS)
    Call<ArticlesResponce> ARTICLES_RESPONCE_CALL(@Body SectionsRequest sectionsRequest);

}


