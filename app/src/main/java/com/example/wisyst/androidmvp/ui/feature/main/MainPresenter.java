package com.example.wisyst.androidmvp.ui.feature.main;

import com.example.wisyst.androidmvp.data.remote.requests.SectionsRequest;
import com.example.wisyst.androidmvp.data.remote.responces.ArticlesResponce;
import com.example.wisyst.androidmvp.network.ApiClient;
import com.example.wisyst.androidmvp.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wisyst on 6/11/2018.
 */

public class MainPresenter {

    private MainView mainView;

    public MainPresenter(MainView mainView) {
        this.mainView = mainView;
    }

    public void getArticlesFromApi() {
        mainView.showProgress();

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        SectionsRequest params = new SectionsRequest();
        params.setId("3");
        params.setType(1);
        apiInterface.ARTICLES_RESPONCE_CALL(params).enqueue(new Callback<ArticlesResponce>() {
            @Override
            public void onResponse(Call<ArticlesResponce> call, Response<ArticlesResponce> response) {
                mainView.hideProgress();
                mainView.onResponseRecevied(response.body());
            }

            @Override
            public void onFailure(Call<ArticlesResponce> call, Throwable t) {
                mainView.hideProgress();
                mainView.onErrorReceived(t.toString());
            }
        });
    }

}
