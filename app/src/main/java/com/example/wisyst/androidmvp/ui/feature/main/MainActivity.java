package com.example.wisyst.androidmvp.ui.feature.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.wisyst.androidmvp.R;
import com.example.wisyst.androidmvp.data.remote.responces.ArticlesResponce;
import com.example.wisyst.androidmvp.ui.shared.adapters.SectionsAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainView {


    LinearLayoutManager linearLayoutManager;
    SectionsAdapter sectionsAdapter;
    MainPresenter mainPresenter;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.pb_data)
    ProgressBar pbData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mainPresenter = new MainPresenter(this);
        mainPresenter.getArticlesFromApi();
    }

    @Override
    public void onResponseRecevied(ArticlesResponce articlesResponce) {
        pbData.setVisibility(View.GONE);
        Toast.makeText(this, "every thing ok", Toast.LENGTH_SHORT).show();
        linearLayoutManager = new LinearLayoutManager(this);
        sectionsAdapter = new SectionsAdapter(articlesResponce.getPosts().getData(), this);
        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(sectionsAdapter);

    }

    @Override
    public void onErrorReceived(String message) {
        Toast.makeText(this, "Some thing went wrong", Toast.LENGTH_SHORT).show();
        pbData.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        pbData.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbData.setVisibility(View.GONE);
    }
}
