package com.example.wisyst.androidmvp.ui.feature.login;

/**
 * Created by wisyst on 6/11/2018.
 */

public interface LoginView {

    void showProgress();

    void hideProgress();

    void setUserNameError();

    void setUserPassError();

    void navigateToHome();
}
