package com.example.wisyst.androidmvp.data.remote.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by wisyst on 3/12/2018.
 */

public class SectionsRequest {
    @SerializedName("type")
    @Expose
    private int type;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("page")
    @Expose
    private int page;

    public void setPage(int page) {
        this.page = page;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setId(String id) {
        this.id = id;
    }
}
