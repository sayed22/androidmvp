package com.example.wisyst.androidmvp.ui.feature.login;

/**
 * Created by wisyst on 6/11/2018.
 */

public interface LoginInteractor {

    interface OnLoginFinishedListener {
        void onUsernameError();

        void onPasswordError();

        void onSuccess();
    }

    void login(String username, String password, OnLoginFinishedListener listener);
}
