package com.example.wisyst.androidmvp.ui.shared.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wisyst.androidmvp.R;
import com.example.wisyst.androidmvp.data.remote.responces.Article;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by wisyst on 3/13/2018.
 */

public class SectionsAdapter extends RecyclerView.Adapter<SectionsAdapter.MyViewHolder> {

    private List<Article> commentList;
    private Context context;
    private LayoutInflater inflater;

    public SectionsAdapter(List<Article> comments, Context context) {
        this.commentList = comments;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SectionsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_writer_article, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SectionsAdapter.MyViewHolder holder, int position) {
        Article article = commentList.get(position);

        MyViewHolder articleVH = (MyViewHolder) holder;

        articleVH.tvViews.setText(article.getPostViewsNumber());
        articleVH.tvDate.setText(article.getPostCreateDate());
        articleVH.tvTitle.setText(article.getPostTitle());
        articleVH.tvBrife.setText(Html.fromHtml(article.getContentBrief()));
        Picasso.with(context)
                .load(article.getPostImageUrl())
                .into(articleVH.ivArticleImage);
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvBrife, tvDate, tvViews;
        private ImageView ivArticleImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivArticleImage = itemView.findViewById(R.id.iv_writer_article);
            tvTitle = itemView.findViewById(R.id.tv_writer_article_title);
            tvBrife = itemView.findViewById(R.id.tv_writer_article_body);
            tvDate = itemView.findViewById(R.id.tv_writer_article_date);
            tvViews = itemView.findViewById(R.id.tv_writer_article_views);
        }
    }
}
