
package com.example.wisyst.androidmvp.data.remote.responces;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArticlesResponce {

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("errNum")
    @Expose
    private int errNum;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("posts")
    @Expose
    private Articles posts;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getErrNum() {
        return errNum;
    }

    public void setErrNum(int errNum) {
        this.errNum = errNum;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Articles getPosts() {
        return posts;
    }

    public void setPosts(Articles posts) {
        this.posts = posts;
    }


}
