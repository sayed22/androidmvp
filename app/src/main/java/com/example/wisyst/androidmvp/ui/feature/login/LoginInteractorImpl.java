package com.example.wisyst.androidmvp.ui.feature.login;

import android.os.Handler;
import android.text.TextUtils;

/**
 * Created by wisyst on 6/11/2018.
 */

public class LoginInteractorImpl implements LoginInteractor {


    @Override
    public void login(final String username, final String password, final OnLoginFinishedListener listener) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(username)) {
                    listener.onUsernameError();
                    return;
                }
                if (TextUtils.isEmpty((password))) {
                    listener.onPasswordError();
                    return;
                }

                listener.onSuccess();
            }
        }, 2000);
    }
}
